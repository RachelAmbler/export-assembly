﻿using System;
using System.IO;
using CommandLine;
using Microsoft.Data.SqlClient;

namespace ExportAssembly
{
        public class Options
    {
        [Option('a', "AssemblyName", Required = true, HelpText = "The name of the assembly to export.")]
        public string AssemblyName { get; set; }
        [Option('h', "HostName", Required = true, HelpText = "The host & instance name (or host & port) of the Sql Server to connect to.")]
        public string HostName { get; set; }
        [Option('d', "DatabaseName", Required = true, HelpText = "The name of the database that contains the assembly.")]
        public string DatabaseName { get; set; }
        [Option('o', "OutputFolder", Required = true, HelpText = "The local folder to export the assembly to.")]
        public string OutputFolder { get; set; }
        [Option('l', "LoginId", Required = false, HelpText = "Login Id to connect as if not using integrated security.")]
        public string LoginId { get; set; }
        [Option('p', "Password", Required = false, HelpText = "Password of the login Id if not using integrated security.")]
        public string Password { get; set; }
        [Option('f', "ForceOverwrite", Required =  false, Default = false, HelpText = "Force an overwrite of the output file if it already exists.")]
        public bool ForceOverwrite { get; set; }
    }
    internal class Program
    {
        public static void Main(string[] args)
        {
            var assemblyName = string.Empty;
            var hostName = string.Empty;
            var databaseName = string.Empty;
            var outputFolder = string.Empty;
            var loginId = string.Empty;
            var password = string.Empty;
            var forceOverwrite = false;
            object res;
            bool fail = false;
            try
            {
                Parser.Default.ParseArguments<Options>(args)
                        .WithNotParsed(e => { fail = true;})
                        .WithParsed<Options>(o =>
                        {
                            assemblyName = o.AssemblyName;
                            hostName = o.HostName;
                            databaseName = o.DatabaseName;
                            outputFolder = o.OutputFolder;
                            loginId = o.LoginId;
                            password = o.Password;
                            forceOverwrite = o.ForceOverwrite;
                        });
            }
            catch { }

            if (fail) return;

            if (!Directory.Exists(outputFolder))
            {
                Console.Error.WriteLine($"Export folder {outputFolder} does not exist.");
                return;
            }
            var outputFile = Path.Combine(outputFolder, assemblyName) + ".dll";

            if (File.Exists(outputFile) && !forceOverwrite)
            {
                Console.Error.WriteLine($"File {outputFile} already exists and --ForceOverwrite (-f) was not set.");
                return;
            }

            const string sql = @"Select Af.content From sys.assembly_files Af Join sys.assemblies A On Af.assembly_id = A.assembly_id Where Af.file_id = 1 And A.name = @AssemblyName";

            var sqlBuilder = new SqlConnectionStringBuilder {DataSource = hostName, InitialCatalog = databaseName};
            if (loginId == string.Empty)
                sqlBuilder.IntegratedSecurity = true;
            else
            {
                sqlBuilder.IntegratedSecurity = false;
                sqlBuilder.UserID = loginId;
                sqlBuilder.Password = password;
            }

            using var connection = new SqlConnection(sqlBuilder.ConnectionString);
            try
            {
                connection.Open();
            }
            catch (SqlException e)
            {
                Console.Error.WriteLine($"Failed to open a connection to {databaseName}@{hostName}");
                Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine(e.StackTrace);
                return;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine($"Unhandled exception in cx.Open()");
                Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine(e.StackTrace);
                return;
            }

            var cmd = connection.CreateCommand();
            cmd.CommandText = sql;
            cmd.Parameters.AddWithValue("@AssemblyName", assemblyName);
            try
            {
                res = cmd.ExecuteScalar();
                if (res == null)
                {
                    Console.Error.WriteLine($"Unable to find assembly {assemblyName} on server {hostName}, database {databaseName}.");
                    return;
                }
            }
            catch (SqlException e)
            {
                Console.Error.WriteLine($"Failed to run the command to extract the byte stream from {databaseName}@{hostName}.");
                Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine(e.StackTrace);
                return;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine($"Unhandled exception in cmd.ExecuteScalar()");
                Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine(e.StackTrace);
                return;
            }
            
            var bytes = (byte[]) res;
            File.WriteAllBytes(outputFile, bytes);
            Console.WriteLine($"Wrote {bytes.Length} bytes to {outputFile}.");
        }
    }
}
