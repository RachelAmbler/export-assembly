# Usage

`-a` `--AssemblyName` **(Required)** The name of the assembly to export.

`-h` `--HostName` **(Required)** The host & instance name (or host & port) of the the Sql Server to connect to.

`-d` `--DatabaseName` **(Required)** The name of the database that contains the assembly.

 `-o` `--OutputFolder` **(Required)** The local folder to export the assembly to.

`-l` `--LoginId` Login Id to connect as if not using integrated.

`-p` `--Password` Password of the login Id if not using integrated security.

 `-f` `--ForceOverwrite`  _(Default: false)_ Force an overwrite of the output file if it already exists.

`--help` Display the help screen.

`--version` Display version information.